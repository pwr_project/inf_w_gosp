package lawFirm.FileSystem;

import java.io.*;
import java.nio.file.*;

public class FileSystem {
	private FileSystem() {}
	
	public static FileSystem instance()
	{
		if(instance == null) {
	    	instance = new FileSystem();
	 	}
		return instance;
	}
	public Path addFile(Path source_file, Path destination_dir) throws IOException
	{
		if(!Files.exists(destination_dir)) {
			Files.createDirectories(destination_dir);
		}
		if(Files.isDirectory(source_file)) {
			return Paths.get("");
		}
		
		Path destination_file = destination_dir.resolve(source_file.getFileName());

	    CopyOption[] options = new CopyOption[]{
	      StandardCopyOption.REPLACE_EXISTING,
	      StandardCopyOption.COPY_ATTRIBUTES
	    }; 

	    return Files.copy(source_file, destination_file, options);
	}
	
	public void removeFile(Path file) throws IOException
	{
		try {
			Files.deleteIfExists(file);
			Files.delete(file.getParent());
		} catch (DirectoryNotEmptyException e) {
			System.out.println("Directory not empty - won't remove.");
		}
	}
	
	private static FileSystem instance;
}
