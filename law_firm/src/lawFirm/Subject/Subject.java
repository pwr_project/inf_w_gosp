package lawFirm.Subject;

import lawFirm.Address;

public abstract class Subject {
	public Subject(SubjectType type, String name, String id)
	{
		this.type = type;
		this.name = name;
		this.id = id;
	}
	
	public void setId(String id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}

	public String getId()
	{
		return this.id;
	}

	public String getName()
	{
		return this.name;
	}

	public Address getAddress()
	{
		return this.address;
	}
	
	public SubjectType getType()
	{
		return this.type;
	}
	
	public abstract String getTypeString();
		
	protected SubjectType type;
	protected String name;
	protected String id;
	protected Address address;
}
