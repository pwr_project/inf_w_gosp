package lawFirm.Subject;

public class Firm extends Subject {
	public Firm(String name, String krs)
	{
		super(SubjectType.FIRM, name, krs);
	}
	
	public String toString() {
		return type_string + ": " + name + ", " + id_type_string + " " + id; 
	}
	
	public String getTypeString()
	{
		return type_string;
	}

	public static String type_string = "Przedsiebiorstwo";
	private static String id_type_string = "KRS";
}
