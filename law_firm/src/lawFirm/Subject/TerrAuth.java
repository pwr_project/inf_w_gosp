package lawFirm.Subject;

public class TerrAuth extends Subject {
	public TerrAuth(String name, String nip)
	{
		super(SubjectType.TERR_AUTH, name, nip);
	}
	
	public String toString() {
		return type_string + ": " + name + ", " + id_type_string + " " + id; 
	}
	
	public String getTypeString()
	{
		return type_string;
	}

	public static String type_string = "Jednostka samorzadu terytorialnego";
	private static String id_type_string = "NIP";
}
