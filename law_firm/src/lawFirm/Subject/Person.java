package lawFirm.Subject;

public class Person extends Subject {
	public Person(String name, String pesel)
	{
		super(SubjectType.PERSON, name, pesel);
	}
	
	
	public void setBirthdate(String birthdate)
	{
		this.birthdate = birthdate;
	}
	
	public String getBirthdate()
	{
		return this.birthdate;
	}
	
	public String toString()
	{
		return type_string + ": " + name + ", " + id_type_string + " " + id; 
	}

	public String getTypeString()
	{
		return type_string;
	}

	public static String type_string = "Osoba prywatna";
	private String birthdate;
	private static String id_type_string = "PESEL";
}
