package lawFirm.Subject;

public enum SubjectType
{
	PERSON("Osoba fizyczna"), TERR_AUTH("Jednostka samorzadu terytorialnego"), FIRM("Firma");

	private String displayName;
	private SubjectType(String display)
	{
		this.displayName = display;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	@Override
	public String toString() {
		return this.displayName;
	}

}
