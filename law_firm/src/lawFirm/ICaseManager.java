package lawFirm;

import java.util.List;

import lawFirm.Case.Case;

public interface ICaseManager {
	public abstract void removeCaseFromDb(String id);
	List<SearchCaseResult> searchCasesInDb(String str);
	public abstract Case getCase(int db_id);
	public abstract void addCaseToDb(Case case_);
}
