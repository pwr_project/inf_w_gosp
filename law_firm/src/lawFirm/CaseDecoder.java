package lawFirm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import lawFirm.Case.Attachment;
import lawFirm.Case.Attachments;
import lawFirm.Case.Calendar;
import lawFirm.Case.Case;
import lawFirm.Case.Event;
import lawFirm.Case.Tribunal;
import lawFirm.Subject.Firm;
import lawFirm.Subject.Person;
import lawFirm.Subject.Subject;
import lawFirm.Subject.TerrAuth;

public class CaseDecoder {
	public static Case decode(List<String> msg, Function<String, List<String>> subjectGetter)
	{
		if (msg.size() == 11)
		{
			String id = msg.get(1);
			String type = msg.get(2);
			Tribunal tribunal = decodeTribunal(msg.get(3));
			Subject my_side = decodeSubject(subjectGetter.apply(msg.get(4)));
			Subject opposite_side = decodeSubject(subjectGetter.apply(msg.get(5)));
			String docs_placement = msg.get(6);
			String note = msg.get(7);
			ECaseStatus status = decodeStatus(msg.get(8));
			Calendar calendar = decodeCalendar(msg.get(10));
			Attachments attachments = decodeAttachments(msg.get(9));

			Case result = new Case(id, type, tribunal, my_side, opposite_side, docs_placement, note, calendar, attachments);
			if (status == ECaseStatus.Closed) {
				result.closeCase();
			}
			return result;
		}
		return null;
	}

	private static Tribunal decodeTribunal(String msg)
	{
		if (msg == null) return new Tribunal("", null);
		String[] tmp = msg.split(new String("\\^&\\^"));
		if (tmp.length != 4) { System.out.println("DB read tribunal msg != 4"); return new Tribunal("", null); }
		String name = tmp[0];
		String street = tmp[1];
		String city = tmp[2];
		String code = tmp[3];
		Address address = new Address(street, city, code);
		return new Tribunal(name, address);
	}
	
	private static Subject decodeSubject(List<String> msg)
	{
		if (msg.isEmpty()) return null;
		String dupa = msg.get(3);
		String[] tmp = dupa.split(new String("\\^&\\^"));
		if (tmp.length != 3) return null;
		String street = tmp[0];
		String city = tmp[1];
		String code = tmp[2];
		Address address = new Address(street, city, code);
		Subject result;
		if (msg.get(1) == Firm.type_string) {
			result = new Firm(msg.get(2), msg.get(0));
		} else if (msg.get(1) == TerrAuth.type_string) {
			result = new TerrAuth(msg.get(2), msg.get(0));
		} else {
			result = new Person(msg.get(2), msg.get(0));
		}

		result.setAddress(address);
		return result;
	}
	
	private static ECaseStatus decodeStatus(String msg)
	{
		if (msg.isEmpty()) return null;
		if (msg == "open") {
			return ECaseStatus.Open;
		}
		return ECaseStatus.Closed;
	}
	
	private static Calendar decodeCalendar(String msg)
	{
		if (msg.isEmpty()) return null;
		Calendar calendar = new Calendar();
		String[] tmp = msg.split("\\^&\\^");
		if (tmp.length == 0) return null;
		for(String value : tmp)
		{
			Event event = decodeEvent(value);
			if (event != null) {
				calendar.events.add(event);
			}
		}
		return calendar;
	}
	
	private static Event decodeEvent(String msg)
	{
		if (msg.isEmpty()) return null;
		String[] tmp = msg.split(new String("\\^&\\^"));
		Date date;
		String str_date = "";
		String note = "";
		if(tmp.length <= 2) {
			str_date = tmp[0];
		}
		if (tmp.length == 2) {
			note = tmp[1];
		}

		SimpleDateFormat formatter = new SimpleDateFormat("dd.mm.yyyy hh:mm");
		try {
			date = formatter.parse(str_date);
		} catch (ParseException e) {
			return null;
		}
		return new Event(date, note);
	}
	
	private static Attachments decodeAttachments(String msg)
	{
		if (msg.isEmpty()) return null;
		Attachments attachments = new Attachments();
		String[] tmp = msg.split(new String("\\^&\\^"));
		if (tmp.length == 0) return null;
		for(String value : tmp)
		{
			if (!value.isEmpty()) {
				if(attachments == null) { System.out.println("atachments are null"); return null; }
				if(value == null) { System.out.println("value is null"); return null; }
				attachments.attachments.add(new Attachment(value));
			}
		}
		return attachments;
	}
}
