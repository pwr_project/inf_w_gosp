package lawFirm;

public enum ECaseStatus {
	Open("Otwarta"),
	Closed("Zamknieta");

	private String displayName;

	private ECaseStatus(String display)
	{
		this.displayName = display;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	@Override
	public String toString() {
		return this.displayName;
	}
}
