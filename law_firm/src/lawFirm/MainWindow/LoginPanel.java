package lawFirm.MainWindow;

/**
 * Created by hawelka on 12.12.14.
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class LoginPanel {
    JPanel panel = new JPanel();

    public JButton advocateButton = new JButton("Adwokat");
    public JButton employeeButton = new JButton("Pracownik");
    public JButton traineeButton = new JButton("Praktykant");
    JButton exitButton = new JButton("Wyjdz");

    LoginPanel()
    {
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.setBorder(BorderFactory.createTitledBorder("Zaloguj sie"));

        c.ipady = 20;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(advocateButton, c);
        c.gridy = 1;
        panel.add(employeeButton, c);
        c.gridy = 2;
        panel.add(traineeButton, c);
        c.gridy = 3;
        panel.add(exitButton, c);

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               System.exit(0);
            }
        });
    }


}
