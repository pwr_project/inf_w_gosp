package lawFirm.MainWindow;

/**
 * Created by hawelka on 12.12.14.
 */
import kancelaria.Kancelaria;
import lawFirm.Address;
import lawFirm.Case.*;
import lawFirm.Case.Calendar;
import lawFirm.Case.Event;
import lawFirm.CaseManager;
import lawFirm.ICaseManager;
import lawFirm.Subject.*;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class AddCasePanel {
    JPanel panel = new JPanel();
    JFrame side = new JFrame("Dodaj strone...");
    JPanel addSidePanel = new JPanel();

    JLabel idLabel = new JLabel("Sygnatura sprawy:");
    JLabel typeLabel = new JLabel("Rodzaj sprawy:");
    JLabel tribunalLabel= new JLabel("Sad wlasciwy:");
    JLabel tribunalStreetLbl = new JLabel("Ulica:");
    JLabel tribunalPostcodeLbl = new JLabel("Kod pocztowy i miasto:");
    JLabel mySideLabel = new JLabel("Strona:");
    JLabel oppositeSideLabel = new JLabel("Strona przeciwna:");
    JLabel documentsPlacementLabel = new JLabel("Polozenie akt:");
    JLabel notesLabel = new JLabel("Notatki:");

    JTextField idTextField = new JTextField(40);
    JTextField typeTextField = new JTextField(40);
    JTextField tribunalTextField = new JTextField(40);
    JTextField tribunalStreetTxt = new JTextField(40);
    JTextField tribunalPostcodeTxt = new JTextField(40);
    JTextField tribunalCityTxt = new JTextField(40);

    JButton addMySideButton = new JButton("Dodaj...");
    JButton findMySideButton = new JButton("Szukaj..");
    JTextArea mySideTextArea = new JTextArea();
    JButton addOppositeSideButton = new JButton("Dodaj...");
    JButton findOppositeSideButton = new JButton("Szukaj..");
    JTextArea oppositeSideTextArea = new JTextArea();
    JTextField documentsPlacementTextField = new JTextField(40);
    JTextArea notesTextArea = new JTextArea(1, 5);
    JButton backButton = new JButton("<< Powrot");
    JButton addButton = new JButton("Dodaj >>");

    JButton addAttachmentBtn = new JButton("Dodaj zalacznik...");
    JList<String> attachmentList = new JList<String>();
    ArrayList<String> attList = new ArrayList<String>();

    JLabel sideTypeLabel = new JLabel("Typ strony:");
    JComboBox sideTypeComboBox = new JComboBox(SubjectType.values());
    JLabel sideNameLabel = new JLabel("Imie i nazwisko:");
    JTextField sideNameTextField = new JTextField(40);
    JLabel addressStreetLabel = new JLabel("Ulica:");
    JTextField addressStreetTextField = new JTextField(40);
    JLabel addressCityLabel = new JLabel("Miasto:");
    JTextField addressCityTextField = new JTextField(20);
    JLabel addressPostcodeLabel = new JLabel("Kod pocztowy:");
    JTextField addressPostcodeTextField = new JFormattedTextField(createFormatter("##-###"));
    JLabel sideIdLabel = new JLabel("PESEL:");
    JTextField sideIdTextField = new JTextField(40);
    JLabel birthdayLabel = new JLabel("Data urodzenia:");
    JTextField birthdayTextField = new JFormattedTextField(createFormatter("##-##-####"));
    JButton addMySide = new JButton("Dodaj");
    JButton addOppositeSide = new JButton("Dodaj");

    JLabel dateLbl = new JLabel("Data:");
    JTextField dateTxt = new JTextField(40);

    Subject mySide;
    Subject oppositeSide;

    ICaseManager dbInterface = new CaseManager(new Kancelaria());
    JFileChooser fc = new JFileChooser();
    List<Event> eventList = new Vector<Event>();

    AddCasePanel()
    {
        prepareAddCasePanel();

        sideTypeComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (sideTypeComboBox.getSelectedItem().equals(SubjectType.TERR_AUTH)) {
                    sideIdLabel.setText("NIP:");
                    birthdayLabel.setEnabled(false);
                    birthdayTextField.setEnabled(false);
                } else if (sideTypeComboBox.getSelectedItem().equals(SubjectType.FIRM)) {
                    sideIdLabel.setText("KRS:");
                    birthdayLabel.setEnabled(false);
                    birthdayTextField.setEnabled(false);
                } else {
                    sideIdLabel.setText("PESEL:");
                    birthdayLabel.setEnabled(true);
                    birthdayTextField.setEnabled(true);
                }
            }
        });
        addMySide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //getInput from addSidePanel and give it to the database, then take data from database and show it in textarea
                if(!checkAddSideInput()) {
                    MainWindow.showErrorMsg("Uzupelnij wszystkie pola!", "Bledne dane");
                    return;
                }
                mySide = getAddSideInput();
                mySideTextArea.setText(mySide.getName() + "\n"
                        + mySide.getAddress().toString() + "\n"
                        + mySide.getType().getDisplayName());
                clearAddSidePanel();
                side.dispatchEvent(new WindowEvent(side, WindowEvent.WINDOW_CLOSING));
            }
        });
        addOppositeSide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //getInput from addSidePanel and give it to the database, then take data from database and show it in textarea
                if(!checkAddSideInput()) {
                    MainWindow.showErrorMsg("Uzupelnij wszystkie pola!", "Bledne dane");
                    return;
                }
                oppositeSide = getAddSideInput();
                oppositeSideTextArea.setText(oppositeSide.getName());
                clearAddSidePanel();
                side.dispatchEvent(new WindowEvent(side, WindowEvent.WINDOW_CLOSING));
            }
        });

        addMySideButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAddSidePanel(addMySide);
            }
        });
        addOppositeSideButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAddSidePanel(addOppositeSide);
            }
        });
        addAttachmentBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fc.showOpenDialog(panel);

                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    attList.add(file.getPath());
                    attachmentList.setListData(attList.toArray(new String[attList.size()]));
                }
            }
        });
    }

    /*
     * AddCasePanel methods
     */

    private void prepareAddCasePanel() {
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        panel.setBorder(BorderFactory.createTitledBorder("Dodaj sprawe"));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 15;
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(idLabel, c);
        c.gridy = 1;
        panel.add(typeLabel, c);
        c.gridy = 2;
        panel.add(tribunalLabel, c);
        c.gridy = 3;
        panel.add(tribunalStreetLbl, c);
        c.gridy = 4;
        panel.add(tribunalPostcodeLbl, c);
        c.gridy = 5;
        panel.add(mySideLabel, c);
        c.gridy = 7;
        panel.add(oppositeSideLabel, c);
        c.gridy = 9;
        panel.add(documentsPlacementLabel, c);
        c.gridy = 10;
        panel.add(notesLabel, c);
        c.gridy = 11;
        panel.add(addAttachmentBtn, c);
        c.gridy = 12;
        panel.add(dateLbl, c);
        c.gridy = 13;
        panel.add(backButton, c);

        c.gridx = 2;
        c.gridy = 0;
        panel.add(idTextField, c);
        c.gridy = 1;
        panel.add(typeTextField, c);
        c.gridy = 2;
        panel.add(tribunalTextField, c);
        c.gridy = 3;
        panel.add(tribunalStreetTxt, c);
        c.gridy = 9;
        panel.add(documentsPlacementTextField, c);
        c.gridy = 10;
        panel.add(notesTextArea, c);
        c.gridy = 11;
        panel.add(attachmentList, c);
        c.gridy = 12;
        panel.add(dateTxt, c);
        c.gridy = 13;
        panel.add(addButton, c);

        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 4;
        panel.add(tribunalPostcodeTxt, c);
        c.gridy = 5;
        panel.add(addMySideButton, c);
        c.gridy = 7;
        panel.add(addOppositeSideButton, c);
        c.gridx = 3;
        c.gridy = 4;
        panel.add(tribunalCityTxt, c);
        c.gridy = 5;
        panel.add(findMySideButton, c);
        c.gridy = 7;
        panel.add(findOppositeSideButton, c);

        c.ipady = 40;
        c.gridwidth = 4;
        c.gridx = 0;
        c.gridy = 6;
        panel.add(mySideTextArea, c);
        mySideTextArea.setEditable(false);
        c.gridy = 8;
        panel.add(oppositeSideTextArea, c);
        oppositeSideTextArea.setEditable(false);
    }

    //Checks if all needed fields are filled with data.
    public boolean checkInput()
    {
        if(idTextField.getText().equals("") || tribunalTextField.getText().equals("") || typeTextField.getText().equals("")
                || mySideTextArea.getText().equals("") || oppositeSideTextArea.getText().equals("") ||
                documentsPlacementTextField.getText().equals("") || dateTxt.getText().equals("")) {
            return false;
        }
        return true;
    }

    public Case getInput()
    {
        Tribunal trib = new Tribunal(tribunalTextField.getText(),
                new Address(tribunalStreetTxt.getText(), tribunalCityTxt.getText(), tribunalPostcodeTxt.getText()));
        DateFormat df = new SimpleDateFormat("dd.mm.yyyy hh:mm");
        Calendar calendar = new Calendar();
        try {
            Date date = new Date();
            date = df.parse(dateTxt.getText());
            eventList.add(new Event(date, notesTextArea.getText()));
            calendar.events = eventList;
        } catch(ParseException f) {
            f.printStackTrace();
        }

        try {
            Attachments att = AttachmentsFactory.createAttachments(attList, idTextField.getText());
            return new Case(idTextField.getText(), typeTextField.getText(), trib, mySide, oppositeSide,
                    documentsPlacementTextField.getText(), notesTextArea.getText(), calendar, att);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Case(idTextField.getText(), typeTextField.getText(), trib, mySide, oppositeSide,
                documentsPlacementTextField.getText(), notesTextArea.getText(), calendar, new Attachments());

    }

    public void clearAddCasePanel()
    {
        idTextField.setText("");
        typeTextField.setText("");
        tribunalTextField.setText("");
        documentsPlacementTextField.setText("");
        notesTextArea.setText("");
        mySideTextArea.setText("");
        oppositeSideTextArea.setText("");
    }

    /*
     * AddSidePanel methods
     */

    public void prepareAddSidePanel(JButton addSide)
    {
        addSidePanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        addSidePanel.setBorder(BorderFactory.createTitledBorder("Dodaj strone"));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 15;
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 0;
        addSidePanel.add(sideTypeLabel, c);
        c.gridx = 2;
        addSidePanel.add(sideTypeComboBox, c);
        c.gridx = 0;
        c.gridy = 1;
        addSidePanel.add(sideNameLabel, c);
        c.gridx = 2;
        addSidePanel.add(sideNameTextField, c);
        c.gridx = 0;
        c.gridy = 3;
        addSidePanel.add(sideIdLabel, c);
        c.gridx = 2;
        addSidePanel.add(sideIdTextField, c);
        c.gridx = 0;
        c.gridy = 4;
        addSidePanel.add(birthdayLabel, c);
        c.gridx = 2;
        addSidePanel.add(birthdayTextField, c);
        c.gridx = 0;
        c.gridy = 5;
        addSidePanel.add(addressStreetLabel, c);
        c.gridx = 2;
        addSidePanel.add(addressStreetTextField, c);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 6;
        addSidePanel.add(addressPostcodeLabel, c);
        c.gridx = 1;
        addressPostcodeTextField.setColumns(6);
        addSidePanel.add(addressPostcodeTextField, c);
        c.gridx = 2;
        addSidePanel.add(addressCityLabel, c);
        c.gridx = 3;
        addSidePanel.add(addressCityTextField, c);

        c.ipady = 40;
        c.gridwidth = 4;
        c.gridx = 0;
        c.gridy = 9;
        addSidePanel.add(addSide, c);
    }

    private void showAddSidePanel(JButton addSide)
    {
        prepareAddSidePanel(addSide);
        side.getContentPane().add(addSidePanel, BorderLayout.CENTER);
        side.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        side.pack();
        side.setSize(700, 400);
        side.setVisible(true);
    }

    //Checks if all needed fields are filled with data.
    public boolean checkAddSideInput()
    {
        if(sideNameTextField.getText().equals("") || sideIdTextField.getText().equals("") || addressStreetTextField.getText().equals("")
                || addressPostcodeTextField.getText().equals("") || addressCityTextField.getText().equals("")) {
            return false;
        }
        return true;
    }

    public Subject getAddSideInput()
    {
        Address addr = new Address(
                addressStreetTextField.getText(), addressCityTextField.getText(), addressPostcodeTextField.getText());
        if (sideTypeComboBox.getSelectedItem().equals(SubjectType.TERR_AUTH)) {
            TerrAuth terrAuth = new TerrAuth(sideNameTextField.getText(), sideIdTextField.getText());
            terrAuth.setAddress(addr);
            return terrAuth;
        } else if (sideTypeComboBox.getSelectedItem().equals(SubjectType.FIRM)) {
            Firm firm = new Firm(sideNameTextField.getText(), sideIdTextField.getText());
            firm.setAddress(addr);
            return firm;
        } else {
            Person person = new Person(sideNameTextField.getText(), sideIdTextField.getText());
            person.setBirthdate(birthdayTextField.getText());
            person.setAddress(addr);
            return person;
        }
    }

    public void clearAddSidePanel()
    {
        sideTypeComboBox.setSelectedIndex(0);
        sideNameTextField.setText("");
        sideIdTextField.setText("");
        addressCityTextField.setText("");
        addressPostcodeTextField.setText("");
        addressStreetTextField.setText("");
        birthdayTextField.setText("");
        addSidePanel.remove(addMySide);
        addSidePanel.remove(addOppositeSide);
    }

    protected MaskFormatter createFormatter(String s) {
        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
            System.exit(-1);
        }
        return formatter;
    }
}
