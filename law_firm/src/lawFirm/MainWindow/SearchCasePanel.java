package lawFirm.MainWindow;

/**
 * Created by hawelka on 12.12.14.
 */

import kancelaria.Kancelaria;
import lawFirm.Case.Case;
import lawFirm.CaseManager;
import lawFirm.ICaseManager;
import lawFirm.SearchCaseResult;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Vector;
import java.util.List;

public class SearchCasePanel {
    JPanel panel = new JPanel();
    JFrame frame = new JFrame();

    JTextField searchPhraseTextField = new JTextField(15);
    JButton searchButton = new JButton("Szukaj");
    DefaultListModel<String> model = new DefaultListModel<String>();
    JList<String> resultsList = new JList<String>(model);
    ICaseManager dbInterface = new CaseManager(new Kancelaria());
    List<SearchCaseResult> scResults = new Vector<SearchCaseResult>();
    List<String> scString = new Vector<String>();
    JTextArea resultArea = new JTextArea();
    AddCasePanel showCase = new AddCasePanel();

    SearchCasePanel()
    {
        initializeShowCase();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        panel.setBorder(BorderFactory.createTitledBorder("Szukaj sprawy"));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(searchPhraseTextField, c);

        c.gridx = 3;
        panel.add(searchButton, c);

        c.gridx = 0;
        c.gridy = 1;
        panel.add(resultsList, c);

        c.gridx = 2;
        panel.add(resultArea, c);


        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.clear();
                scString.clear();
                scResults = dbInterface.searchCasesInDb(searchPhraseTextField.getText());
                for(int i = 0; i < scResults.size(); i++) {
                    scString.add(scResults.get(i).toString());
                }
                resultsList.setListData(scString.toArray(new String[scString.size()]));
            }
        });
        resultsList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2) {
                    System.out.println(resultsList.getSelectedIndex());
                    System.out.println(scResults.get(resultsList.getSelectedIndex()));
                    Case chosenCase = dbInterface.getCase(scResults.get(resultsList.getSelectedIndex()).getDatabaseLocation());
                    showFoundCase(chosenCase);
                }
            }
        });
    }

    public void initializeShowCase()
    {
        showCase.idTextField.setEditable(false);
        showCase.typeTextField.setEditable(false);
        showCase.tribunalTextField.setEditable(false);
        showCase.tribunalCityTxt.setEditable(false);
        showCase.tribunalPostcodeTxt.setEditable(false);
        showCase.tribunalStreetTxt.setEditable(false);
        showCase.mySideTextArea.setEditable(false);
        showCase.oppositeSideTextArea.setEditable(false);
        showCase.addMySideButton.setEnabled(false);
        showCase.findMySideButton.setEnabled(false);
        showCase.addOppositeSideButton.setEnabled(false);
        showCase.findOppositeSideButton.setEnabled(false);
        showCase.addAttachmentBtn.setEnabled(false);
        showCase.addButton.setEnabled(false);
        showCase.backButton.setEnabled(false);
    }

    private void showFoundCase(Case chosenCase)
    {
        showCase.idTextField.setText(chosenCase.id);
        showCase.typeTextField.setText(chosenCase.type);
        showCase.tribunalTextField.setText(chosenCase.tribunal.name);
        showCase.tribunalCityTxt.setText(chosenCase.tribunal.address.getCity());
        showCase.tribunalPostcodeTxt.setText(chosenCase.tribunal.address.getPost_code());
        showCase.tribunalStreetTxt.setText(chosenCase.tribunal.address.getAddress());
        showCase.mySideTextArea.setText(chosenCase.my_side.getName() + "\n"
                + chosenCase.my_side.getAddress().toString() + "\n"
                + chosenCase.my_side.getType().getDisplayName());
        showCase.oppositeSideTextArea.setText(chosenCase.opposite_side.getName() + "\n"
                + chosenCase.opposite_side.getAddress().toString() + "\n"
                + chosenCase.opposite_side.getType().getDisplayName());
        showCase.documentsPlacementTextField.setText(chosenCase.documents_placement);
        showCase.notesTextArea.setText(chosenCase.note);
        showCase.dateTxt.setText(chosenCase.calendar.events.get(0).printDate());
        frame.getContentPane().add(showCase.panel, BorderLayout.CENTER);
        frame.pack();
        frame.setSize(500, 900);
        frame.setVisible(true);
    }
}
