package lawFirm.MainWindow;

/**
 * Created by hawelka on 11.12.14.
 */
import kancelaria.Kancelaria;
import lawFirm.ICaseManager;
import lawFirm.CaseManager;
import lawFirm.Reminder;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MainWindow {

    EAccessLevel accessLevel;
    static JFrame mainFrame = new JFrame("Case Archiver");
    JPanel panel = new JPanel();
    CommonMenuBar commonMenu = new CommonMenuBar();

    LoginPanel loginPanel = new LoginPanel();
    MainMenuPanel mainMenuPanel = new MainMenuPanel();
    AddCasePanel addCasePanel = new AddCasePanel();
    SearchCasePanel searchCasePanel = new SearchCasePanel();

    CardLayout cards = new CardLayout();

    ICaseManager dbInterface = new CaseManager(new Kancelaria());
    boolean isLogged = false;

    public MainWindow()
    {
        panel.setLayout(cards);

        panel.add(loginPanel.panel, "1");
        panel.add(mainMenuPanel.panel, "2");
        panel.add(addCasePanel.panel, "3");
        panel.add(searchCasePanel.panel, "4");
        cards.show(panel, "1");

        loginPanel.advocateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                accessLevel = EAccessLevel.Advocate;
                mainMenuPanel.prepareMenu(accessLevel);
                isLogged = true;
                showPanelWithSize("2", 250, 300);
            }
        });

        loginPanel.employeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                accessLevel = EAccessLevel.Employee;
                mainMenuPanel.prepareMenu(accessLevel);
                isLogged = true;
                showPanelWithSize("2", 250, 300);
            }
        });

        loginPanel.traineeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                accessLevel = EAccessLevel.Trainee;
                mainMenuPanel.prepareMenu(accessLevel);
                isLogged = true;
                showPanelWithSize("2", 250, 300);
            }
        });

        mainMenuPanel.addCaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showPanelWithSize("3", 500, 900);
            }
        });

        mainMenuPanel.searchCaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showPanelWithSize("4", 500, 400);
            }
        });

        mainMenuPanel.backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                showPanelWithSize("1", 250, 300);
            }
        });

        addCasePanel.backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addCasePanel.clearAddCasePanel();
                showPanelWithSize("2", 250, 300);
            }
        });
        addCasePanel.addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!addCasePanel.checkInput()) {
                    showErrorMsg("Uzupelnij wszystkie pola!", "Bledne dane");
                    return;
                }
                dbInterface.addCaseToDb(addCasePanel.getInput());
                addCasePanel.clearAddCasePanel();
                showPanelWithSize("2", 250, 300);
            }
        });

        commonMenu.mainMenuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(isLogged) {
                    showPanelWithSize("2", 250, 300);
                } else {
                    showErrorMsg("Nie jestes zalogowany!", "Blad logowania");
                }
            }
        });
        searchCasePanel.searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //dbInterface.findCaseInDb(searchCasePanel.searchPhraseTextField.getText());
            }
        });

        mainFrame.getContentPane().add(commonMenu.panel, BorderLayout.NORTH);
        mainFrame.getContentPane().add(panel, BorderLayout.CENTER);
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setSize(250, 300);
        mainFrame.setVisible(true);

    }

    public static void main(String[] args) {
    	(new Thread(new Reminder())).start();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainWindow();
            }
        });
    }

    static void showErrorMsg(String info, String shortDesc)
    {
        JOptionPane.showMessageDialog(mainFrame, info, shortDesc, JOptionPane.ERROR_MESSAGE);
    }

    private void showPanelWithSize(String panelNumber, int sizex, int sizey)
    {
        cards.show(panel, panelNumber);
        mainFrame.setSize(sizex, sizey);
    }
}
