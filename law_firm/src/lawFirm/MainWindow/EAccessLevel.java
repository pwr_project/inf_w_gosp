package lawFirm.MainWindow;

/**
 * Created by hawelka on 12.12.14.
 */
public enum EAccessLevel {
    Advocate,
    Employee,
    Trainee
}
