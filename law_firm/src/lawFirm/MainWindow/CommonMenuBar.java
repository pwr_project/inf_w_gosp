package lawFirm.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by hawelka on 12.12.14.
 */
public class CommonMenuBar {
    JPanel panel = new JPanel();

    JMenuBar commonMenuBar = new JMenuBar();
    JMenuItem mainMenuButton = new JMenuItem("Menu glowne");
    JMenuItem exitButton = new JMenuItem("Zakoncz");

    CommonMenuBar()
    {
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        commonMenuBar.add(mainMenuButton);
        commonMenuBar.add(exitButton);
        panel.add(commonMenuBar);
    }
}
