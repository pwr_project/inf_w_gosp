package lawFirm.MainWindow;

import javax.swing.*;
import java.awt.*;

/**
 * Created by hawelka on 12.12.14.
 */
public class MainMenuPanel {
    JPanel panel = new JPanel();
    public JButton addCaseButton = new JButton("Dodaj sprawe");
    JButton searchCaseButton = new JButton("Szukaj sprawy");
    JButton editCaseButton = new JButton("Edytuj sprawe");
    JButton removeCaseButton = new JButton("Usun sprawe");
    public JButton backButton = new JButton("<< Powrot do logowania");

    MainMenuPanel()
    {
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.setBorder(BorderFactory.createTitledBorder("Menu glowne"));
        c.ipady = 20;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(addCaseButton, c);
        c.gridy = 1;
        panel.add(searchCaseButton, c);
        c.gridy = 2;
        panel.add(backButton, c);
    }

    public void prepareMenu(EAccessLevel accessLevel) {
        if (accessLevel == EAccessLevel.Trainee) {
            addCaseButton.setEnabled(false);
            editCaseButton.setEnabled(false);
            removeCaseButton.setEnabled(false);
        } else {
            addCaseButton.setEnabled(true);
        }
    }
}
