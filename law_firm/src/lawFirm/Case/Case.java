package lawFirm.Case;

import lawFirm.ECaseStatus;
import lawFirm.Subject.Subject;

public class Case {
	public Case(String id)
	{
		this.id = id;
	}
	
	public Case(String id,
	            String type,
	            Tribunal tribunal,
	            Subject my_side,
	            Subject opposite_side,
	            String docs_placement,
	            String note,
	            Calendar calendar,
	            Attachments attachments)
	{
		this.id = id;
		this.type = type;
		this.tribunal = tribunal;
		this.my_side = my_side;
		this.opposite_side = opposite_side;
		this.documents_placement = docs_placement;
		this.note = note;
		this.status = ECaseStatus.Open;
		this.calendar = calendar;
		this.attachments = attachments;
	}
	
	public void closeCase()
	{
		status = ECaseStatus.Closed;
	}
	
	public String id;
	public String type;
	public Tribunal tribunal;
	public Subject my_side;
	public Subject opposite_side;
	public String documents_placement;
	public String note;
	public ECaseStatus status;
	public Calendar calendar;
	public Attachments attachments;
}
