package lawFirm.Case;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AttachmentsFactory {
	public static Attachments createAttachments(List<String> paths, String case_id) throws IOException
	{
		List<Attachment> attachments = new ArrayList<Attachment>();
		for (String path : paths) {
		    attachments.add(new Attachment(path, case_id));
		}
		
		Attachments result = new Attachments();
		result.attachments = attachments;
		return result;
	}

	private AttachmentsFactory() {}
}
