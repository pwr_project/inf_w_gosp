package lawFirm.Case;

import lawFirm.Address;

public class Tribunal {
	public Tribunal(String name, Address address)
	{
		this.name = name;
		this.address = address;
	}
	
	public String serialize()
	{
		return new String(name + "^&^" + address.serialize());
	}
	
	public String toString()
	{
		return new String(name + " " + address.toString());
	}

	public String name;
	public Address address;
}
