package lawFirm.Case;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import lawFirm.FileSystem.FileSystem;

public class Attachment {
	public Attachment(String file_path, String case_id) throws IOException
	{
		this.path = FileSystem.instance().addFile(getSourceFilePath(file_path), getDestinationDirectoryPath(case_id));
	}
	
	public Attachment(String existing_attachment)
	{
		this.path = getSourceFilePath(existing_attachment);
	}
	
	public String toString()
	{
		return path.toString();
	}

	public void remove() throws IOException
	{
		FileSystem.instance().removeFile(path);
	}
	
	private Path getSourceFilePath(String file_path)
	{
		System.out.println(file_path);
		return Paths.get(file_path.trim());
	}
	
	private Path getDestinationDirectoryPath(String case_id)
	{
		return Paths.get("law_firm\\" + case_id);
	}

	private Path path;
}
