package lawFirm.Case;

import java.util.List;
import java.util.Vector;

public class Calendar {
	public Calendar()
	{
		events = new Vector<Event>();
	}

	public String toString()
	{
		String output = "";
		for(Event event : events)
		{
			output = output + event.toString() + "^&^";
		}
		return output;
	}

	public List<Event> events;
}
