package lawFirm.Case;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Event {
	public Event(Date date, String note)
	{
		this.date = date;
		this.note = note;
	}
	
	public Date date;
	public String note;
	
	public String printDate()
	{
		SimpleDateFormat formattedDate = new SimpleDateFormat ("dd.mm.yyyy hh:mm");
        return formattedDate.format(date);
	}
	
	public String serialize()
	{
		return new String(printDate() + "^%^" + note);
	}
	
	public String toString()
	{
		return new String(printDate() + " - " + note);
	}
}
