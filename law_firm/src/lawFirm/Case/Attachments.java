package lawFirm.Case;

import java.util.List;
import java.util.Vector;

public class Attachments {
	public Attachments() { attachments = new Vector<Attachment>(); }
	
	public List<Attachment> attachments;

	public String toString()
	{
		String output = "";
		for(Attachment att : attachments)
		{
			output = output + att.toString() + "^&^";
		}
		return output;
	}
}
