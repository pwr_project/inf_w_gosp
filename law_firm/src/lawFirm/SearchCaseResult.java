package lawFirm;

public class SearchCaseResult {
	public SearchCaseResult(String id, int db_id)
	{
		this.id = id;
		this.db_id = db_id;
	}
	
	public String toString()
	{
		return id;
	}
	
	public String getId()
	{
		return toString();
	}
	
	public int getDatabaseLocation()
	{
		return db_id;
	}
	
	private String id;
	private int db_id;
}
