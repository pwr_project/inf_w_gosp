package lawFirm;

import java.util.List;
import java.util.Vector;



import kancelaria.Kancelaria;
import lawFirm.Case.*;

public class CaseManager implements ICaseManager
{
	public CaseManager(Kancelaria dataBase)
	{
		this.dataBase = dataBase;
	}

	@Override
	public void addCaseToDb(Case case_)
	{
		dataBase.insertKlient(case_.my_side.getId(),
				              case_.my_side.getTypeString(),
				              case_.my_side.getName(),
				              case_.my_side.getAddress().serialize());

		dataBase.insertKlient(case_.opposite_side.getId(),
				              case_.opposite_side.getTypeString(),
				              case_.opposite_side.getName(),
				              case_.opposite_side.getAddress().serialize());

		dataBase.insertSprawa(case_.id,
				              case_.type,
				              case_.tribunal.serialize(),
				              case_.my_side.getId(),
				              case_.opposite_side.getId(),
				              case_.documents_placement,
				              case_.note,
				              "open",
				              case_.attachments.toString(),
				              case_.calendar.toString());
	}
	
	@Override
	public List<SearchCaseResult> searchCasesInDb(String str)
	{
		List<String> findings = dataBase.selectSprawa(str);
		if (findings == null) { System.out.println("Database returned null"); return null; }
		List<SearchCaseResult> results = new Vector<SearchCaseResult>();
		for (String i : findings) {
			String[] tmp = i.split(";");
			String case_id = tmp[1];
			int db_id = Integer.parseInt(tmp[0]);

			results.add(new SearchCaseResult(case_id, db_id));
		}
		return results;
	}

	@Override
	public Case getCase(int db_id)
	{
		List<String> case_string = dataBase.getSprawa(db_id);
		return CaseDecoder.decode(case_string, (subject_id) -> { return dataBase.selectKlient(subject_id);} );
	}

	@Override
	public void removeCaseFromDb(String id)
	{
		//dataBase.deleteSprawa(id);
	}

	private Kancelaria dataBase;
}
