package lawFirm;

public class Address {
	public Address()
	{}
	
	public Address(String street, String city, String post_code)
	{
		System.out.println("Make addres: " + toString());
		this.address = street;
		this.city = city;
		this.post_code = post_code;
	}
	
	public void setStreetAndLocal(String street)
	{
		this.address = street;
	}
	
	public void setCity(String city)
	{
		this.city = city;
	}
	
	public void setPostCode(String post_code)
	{
		this.post_code = post_code;
	}
	
	public String toString()
	{
		return new String(address + ", " + post_code + " " + city);
	}
	
	public String serialize()
	{
		return new String(address + "^&^" + city + "^&^" + post_code);
	}

	public String getCity() {
		return city;
	}

	public String getAddress() {
		return address;
	}

	public String getPost_code() {
		return post_code;
	}

	private String city;
	private String address;
	private String post_code;
}
