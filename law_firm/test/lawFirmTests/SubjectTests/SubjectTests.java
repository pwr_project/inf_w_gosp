package lawFirmTests.SubjectTests;

import static org.junit.Assert.assertEquals;
import lawFirm.Subject.Firm;
import lawFirm.Subject.Person;
import lawFirm.Subject.Subject;
import lawFirm.Subject.TerrAuth;

import org.junit.Test;

public class SubjectTests {
	// Constants::Names
	private String personName = "Maciek Kwiecien";
	private String firmName = "NSN";
	private String terrAuthName = "Gmina Wroclaw";
	
	// Constants::IDs
	private String id = "12456789";
	private String personIdType = "PESEL";
	private String firmIdType = "KRS";
	private String terrAuthIdType = "NIP";

	@Test
	public void createPersonTest() {
		Subject subject = new Person(personName, id);
		assertEquals(new String(subject.getTypeString() + ": " + personName + ", " + personIdType + " " + id), subject.toString());
	}

	@Test
	public void createFirmTest() {
		Subject subject = new Firm(firmName, id);
		assertEquals(new String(subject.getTypeString() + ": " + firmName + ", " + firmIdType + " " + id), subject.toString());
	}

	@Test
	public void createTerrAuthTest() {
		Subject subject = new TerrAuth(terrAuthName, id);
		assertEquals(new String(subject.getTypeString() + ": " + terrAuthName + ", " + terrAuthIdType + " " + id), subject.toString());
	}
}
