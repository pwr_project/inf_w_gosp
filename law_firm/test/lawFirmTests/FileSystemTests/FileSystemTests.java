package lawFirmTests.FileSystemTests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lawFirm.FileSystem.FileSystem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FileSystemTests {
	protected static String testDataDir = "data_FileSystemTest";
	protected static String exampleFileName = "example.txt";
	protected static String exampleFilePath = testDataDir + "\\" + exampleFileName;
	@Before
	public void setUp() throws Exception {
		Files.createDirectories(Paths.get(testDataDir));
		
		Writer writer = null;

		try {
		    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(exampleFilePath), "utf-8"));
		    writer.write("Something");
		} catch (IOException ex) {
			System.out.println("Failed to create test file " + exampleFilePath);
		} finally {
		   try {writer.close();} catch (Exception ex) {}
		}
	}

	@After
	public void tearDown() throws Exception {
		Files.delete(Paths.get(exampleFilePath));
		Files.delete(Paths.get(testDataDir));
	}

	@Test
	public void fileSystemTest() throws IOException {
		Path destinationDir = Paths.get(testDataDir + "\\1");
		Path destinationFile = destinationDir.resolve(Paths.get(exampleFileName));
		assertFalse(Files.exists(destinationFile));
		FileSystem.instance().addFile(Paths.get(exampleFilePath), destinationDir);	
		FileSystem.instance().addFile(Paths.get(exampleFilePath), destinationDir);
		assertTrue(Files.exists(destinationFile));
		
		FileSystem.instance().removeFile(destinationFile);
		assertFalse(Files.exists(destinationFile));
		assertFalse(Files.exists(destinationDir));
	}
}
