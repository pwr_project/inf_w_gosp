package db_integration;

public class law_third {
	private int idSprawa;
	private int idAutor;
	
	public int getIDSprawa(){
		return idSprawa;
	}
	public void setIDSprawa(int idSprawa){
		this.idSprawa = idSprawa;
	}
	public int getIDAutor(){
		return idAutor;
	}
	public void setIDAutor(int idAutor){
		this.idAutor = idAutor;
	}
	
	public law_third(){}
	public law_third(int idSprawa, int idAutor){
		this.idSprawa = idSprawa;
		this.idAutor = idAutor;
	}
}
