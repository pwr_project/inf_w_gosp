package db_integration;

public class lawFirm {
	private int sygnatura;
	private String rodzajSprawy;
	private String Sad_wlasciwy;
	private String strona;
	private String polozenie;
	private String notatki;
	private String status;
	
	public int getSygnatura(){
		return sygnatura;
	}
	public void setSygnatura(int sygnatura){
		this.sygnatura = sygnatura;
	}
	
	public String getRodzaj(){
		return rodzajSprawy;
	}
	public void setRodzaj(String rodzajSprawy){
		this.rodzajSprawy = rodzajSprawy;
	}
	
	public String getSad(){
		return Sad_wlasciwy;
	}
	public void setSad(String Sad_wlasciwy){
		this.Sad_wlasciwy = Sad_wlasciwy;
	}
	
	
	public String getStrona(){
		return strona;
	}
	public void setStrona(String strona){
		this.strona = strona;
	}
	
	public String getPolozenie(){
		return polozenie;
	}
	public void setPolozenie(String polozenie){
		this.polozenie = polozenie;
	}
	
	public String getNotes(){
		return notatki;
	}
	public void setNotes(String notatki){
		this.notatki = notatki;
	}
	public String getStatus(){
		return status;
	}
	public void setStatus(String status){
		this.status = status;
	}
	

}
