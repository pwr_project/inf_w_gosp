package kancelaria;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

//import db_integration.law_first;
//import db_integration.law_second;


public class Kancelaria {
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:kancelaria.db";
	
	private Connection conn;
	private Statement stat;
	
	public Kancelaria(){

		Connection c = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
		} catch (Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

	    System.out.println("Opened database successfully");
	
	    
	    //!!!!! W razie: KLIENCI*/SPRAWA* already exist prosze o zakomentowanie ponizszych:!!!!!!!!!/
		//createTables();
		//createTables2();
//		
		
	}
	
	//CREATE TABLE IF NOT EXIST
	public void createTables(){
		Connection c = null;
		Statement stmt = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			stmt = c.createStatement();
			String sql1 = "CREATE TABLE KLIENCI16 " +
							"(id VARCHAR(255) PRIMARY KEY," +
							" typ varchar(255)," +
							" name varchar(255)," +
							" adres varchar(255))";
			
							
//			String sql2 = "CREATE TABLE SPRAWA7 " +
//					"(id INTEGER PRIMARY KEY," +
//					" numersprawy varchar(255)," +
//					" rodzajsprawy varchar(255)," +
//					" sadwlasciwy varchar(255)," +
//					" strona varchar(255)," +
//					" stronaprzeciwna varchar(255)," +
//					" polozenie varchar(255)," +
//					" notatki varchar(255)," +
//					" status varchar(255))," +
//					" zalacznik varchar(255)," +
//					" kalendarz varchar(255))";
			stmt.executeUpdate(sql1);
//			stmt.executeUpdate(sql2);
			stmt.close();
			c.close();
		} catch (Exception e){
			System.err.println( e.getClass().getName() + ":w table klientci " + e.getMessage() );
		      System.exit(0);
		}
		System.out.println("Table KLIENCI created successfully");
	}
	
	public void createTables2(){
		Connection c = null;
		Statement stmt = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			stmt = c.createStatement();
			String sql2 = "CREATE TABLE SPRAWA8 " +
					"(id INTEGER PRIMARY KEY AUTOINCREMENT," +
					" numersprawy varchar(255)," +
					" rodzajsprawy varchar(255)," +
					" sadwlasciwy varchar(255)," +
					" strona varchar(255)," +
					" stronaprzeciwna varchar(255)," +
					" polozenie varchar(255)," +
					" notatki varchar(255)," +
					" status varchar(255)," +
					" zalacznik varchar(255)," +
					" kalendarz varchar(255))";
			
			stmt.executeUpdate(sql2);
			stmt.close();
			c.close();
		} catch (Exception e){
			System.err.println( e.getClass().getName() + ":w table sprawa " + e.getMessage() );
		      System.exit(0);
		}
		System.out.println("Table SPRAWA created successfully");
		}

	//INSERT KLIENT - podane zostaja id, typ, name, adres
	//Dodje do tabeli KLIENCI10
	public void insertKlient(String id, String type, String name, String adress)
	  {
	    Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:test.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "INSERT INTO KLIENCI16 (ID,TYP,NAME,ADRES) " +
	                   "VALUES ('"+id+"','"+type+"','"+name+"','"+adress+"');"; 
	      stmt.executeUpdate(sql);
/*
	      sql = "INSERT INTO KLIENCI10 (ID,IMIE,NAZWISKO,PESEL) " +
	            "VALUES (2, 'Allen', 'fujfuj', 15000.00 );"; 
	      stmt.executeUpdate(sql);

	      sql = "INSERT INTO KLIENCI10 (ID,IMIE,NAZWISKO,PESEL) " +
	            "VALUES (3, 'Teddy', 'blabla', 20000.00 );"; 
	      stmt.executeUpdate(sql);

	      sql = "INSERT INTO KLIENCI10 (ID,IMIE,NAZWISKO,PESEL) " +
	            "VALUES (4, 'Mark', 'Winski', 65000.00 );"; 
	      stmt.executeUpdate(sql);
	      //typ, imie, adress, 
*/

	      stmt.close();
	      c.commit();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ":w insertklient " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Records in KLIENCI created successfully");
	  }
	
	
	//INSERT SPRAWA - przyjmuje w ciul kejsow ktore sa ponizej
	//Dodaje do tabeli SPRAWA3
	public void insertSprawa(String numerSprawy, String rodzajSprawy, String sadWlasciwy, String strona, String stronaPrzeciwna,String polozenie, String notatki, String status, String zalacznik, String kalendarz )
	{
		Connection c = null;
		Statement stmt = null;
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			c.setAutoCommit(false);
			System.out.println("Opened database sprawa successfully");
			stmt = c.createStatement();
			String sql = "INSERT INTO SPRAWA8 (ID,NUMERSPRAWY,RODZAJSPRAWY,SADWLASCIWY,STRONA,STRONAPRZECIWNA,POLOZENIE,NOTATKI,STATUS,ZALACZNIK,KALENDARZ) " +
							"VALUES (NULL,'"+numerSprawy+"','"+rodzajSprawy+"','"+sadWlasciwy+"','"+strona+"','"+stronaPrzeciwna+"','"+polozenie+"','"+notatki+"','"+status+"','"+zalacznik+"','"+kalendarz+"');";
			stmt.executeUpdate(sql);
			stmt.close();
			c.commit();
			c.close();
		} catch (Exception e){
			System.err.println( e.getClass().getName() + ":w insersprawa " + e.getMessage() );
		}
		System.out.println("Records in SPRAWA created successfully");
		
	}


	//SELECT SPRAWA - Maciek podaje mi dowolnego stringa jako numer sprawy - ja sprawdzam w kolumnie numersprawy
	//Wynik returnowany w postaci (id;numersprawy) - oczywiscie jako string
	  public List<String> selectSprawa(String str){
		  List<String> results = new Vector<String>();
		  //Connection c = null;
		  //Statement stmt = null;
		  try{
			  Class.forName("org.sqlite.JDBC");
			  conn = DriverManager.getConnection("jdbc:sqlite:test.db");
			  conn.setAutoCommit(false);
			  System.out.println("Opened database successfully");
			  stat = conn.createStatement();
			  ResultSet rs = stat.executeQuery("SELECT * FROM SPRAWA8 WHERE NUMERSPRAWY like '%"+str+"%';");
			 String id, numersprawy;
			  while(rs.next()){
				  id = rs.getString("id");
				  numersprawy = rs.getString("numersprawy");
				  
				  results.add(new String(id + ";" + numersprawy));
			  		}
			  
			  
			  rs.close();
//			  stmt.close();
//			  c.close();
			  
//			  return results;
		  	} catch(Exception e){
					System.err.println( e.getClass().getName() + ": " + e.getMessage() );
					
					System.exit(0);
				  }
				  System.out.println("Operation for selectSprawa done successfully"); 
				  return results;
	  }
	  
	  
	  
		  
	  
/*	  
	  public List<String> selectSprawaPrzezKlienta(String id, String numerSprawy){
		  List<String> results = new Vector<String>();
		  Connection c = null;
		  Statement stmt = null;
		  try{
			  Class.forName("org.sqlite.JDBC");
			  c = DriverManager.getConnection("jdbc:sqlite:test.db");
			  c.setAutoCommit(false);
			  System.out.println("Opened database successfully");
			  ResultSet rs = stmt.executeQuery("SELECT '"+numerSprawy+"' FROM SPRAWA;");
			  while(rs.next()){
				  results.add(new String(id + ";" + numerSprawy));
			  }
			  rs.close();
			  stmt.close();
			  c.close();
		  } catch(Exception e){
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				//System.exit(0);
			  }
			  System.out.println("Operation for sprawa done successfully"); 
	  }
	  
	*/ 

	  //SELECT KLIENT - Maciek podaje mi id ktory jest peselem, KRS, KRD, NIPem, SRIPEM i ja robie SELECT w KLIENTACH w kolumnie ID
	  //Ale wyrzucam calego klienta jako kazde z osobna: id, typ, name, adres
	  public List<String> selectKlient(String id){
		  List<String> results = new Vector<String>();
		  //Connection c = null;
		  //Statement stmt = null;
		  try{
			  Class.forName("org.sqlite.JDBC");
			  conn = DriverManager.getConnection("jdbc:sqlite:test.db");
			  conn.setAutoCommit(false);
			  System.out.println("Opened database successfully");
			  stat = conn.createStatement();
			  ResultSet rs = stat.executeQuery("SELECT * FROM KLIENCI16 WHERE ID LIKE '%"+id+"%';");
			  String typ, name, adres;
			  while(rs.next()){
				  id = rs.getString("ID");
				  typ = rs.getString("TYP");
				  name = rs.getString("NAME");
				  adres = rs.getString("ADRES");
				  
				  
				  results.add(new String(id));
				  results.add(new String (typ));
				  results.add(new String (name));
				  results.add(new String (adres));
			  }
			  rs.close();
//			  stmt.close();
//			  c.close();
			  
//			  return results;
		  } catch(Exception e){
			  System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				System.exit(0);
			  }
			  System.out.println("Operation for selectKlient done successfully");
			 // return null;
			  return results;
		  
	  }
	  
		  
	//GET SPRAWA - MAciej podaje mi int'a, ja szukam w tabeli SPRAWA, w kolumnie ID
	  //Zwracam mu CALA SPRAWE (tak samo jak klientow - kazdy kejs osobno)
	  
	  public List<String> getSprawa(int str){
		  List<String> results = new Vector<String>();
		  //Connection c = null;
		  //Statement stmt = null;
		  try{
			  Class.forName("org.sqlite.JDBC");
			  conn = DriverManager.getConnection("jdbc:sqlite:test.db");
			  conn.setAutoCommit(false);
			  System.out.println("Opened database successfully");
			  stat = conn.createStatement();
			  ResultSet rs = stat.executeQuery("SELECT * FROM SPRAWA8 WHERE ID LIKE '%"+str+"%';");
			  String numersprawy, rodzajsprawy, sadwlasciwy, strona, stronaprzeciwna, polozenie, notatki, status, zalacznik, kalendarz;
			  String id;
			  while(rs.next()){
				  
				  id = rs.getString("id");
				  numersprawy = rs.getString("numersprawy");
				  rodzajsprawy = rs.getString("rodzajsprawy");
				  sadwlasciwy = rs.getString("sadwlasciwy");
				  strona = rs.getString("strona");
				  stronaprzeciwna = rs.getString("stronaprzeciwna");
				  polozenie = rs.getString("polozenie");
				  notatki = rs.getString("notatki");
				  status = rs.getString("status");
				  zalacznik = rs.getString("zalacznik");
				  kalendarz = rs.getString("kalendarz");
				  
				  results.add(new String (id));
				  results.add(new String (numersprawy));
				  results.add(new String (rodzajsprawy));
				  results.add(new String (sadwlasciwy));
				  results.add(new String (strona));
				  results.add(new String (stronaprzeciwna));
				  results.add(new String (polozenie));
				  results.add(new String (notatki));
				  results.add(new String (status));
				  results.add(new String (zalacznik));
				  results.add(new String (kalendarz));
			  }
			  rs.close();
//			  stmt.close();
//			  c.close();
			  
//			  return results;
		  } catch(Exception e){
			  System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				System.exit(0);
			  }
			  System.out.println("Operation for getSprawa done successfully"); 
			  //return null;
			  return results;

		  }
			  
/*
	  public List<String> deleteSprawa(String str){
		  List<String> results = new Vector<String>();
		  Connection c = null;
		  Statement stmt = null;
		  try{
			  Class.forName("org.sqlite.JDBC");
			  c = DriverManager.getConnection("jdbc:sqlite:test.db");
			  c.setAutoCommit(false);
			  System.out.println("Opened database successfully");
			  ResultSet rs = stmt.executeQuery("DELETE FROM SPRAWA3 WHERE NUMERSPRAWY LIKE %'"+str+"'%");
			  String numersprawy, rodzajsprawy, sadwlasciwy, strona, stronaprzeciwna, polozenie, notatki, status, zalacznik, kalendarz;
			  String id;
			  while(rs.next()){
				  id = rs.deleteRow("ID");
				  numersprawy = rs.deleteRow("NUMERSPRAWY");
				  rodzajsprawy = rs.deleteRow("RODZAJSPRAWY");
				  sadwlasciwy = rs.deleteRow("SADWLASCIWY");
				  strona = rs.deleteRow("STRONA");
				  stronaprzeciwna = rs.deleteRow("STRONAPRZECIWNA");
				  polozenie = rs.deleteRow("POLOZENIE");
				  notatki = rs.deleteRow("NOTATKI");
				  status = rs.deleteRow("STATUS");
				  zalacznik = rs.deleteRow("ZALACZNIK");
				  kalendarz = rs.deleteRow("KALENNDARZ");
				  
				  results.remove(new String(id));
				  results.remove(new String(numersprawy));
				  results.remove(new String(rodzajsprawy));
				  results.remove(new String (sadwlasciwy));
				  results.remove(new String (strona));
				  results.remove(new String (stronaprzeciwna));
				  results.remove(new String (polozenie));
				  results.remove(new String (notatki));
				  results.remove(new String (status));
				  results.remove(new String (zalacznik));
				  results.remove(new String (kalendarz));
				   
			  }
				  
				  rs.close();
				  stmt.close();
				  c.close();
				  
				  return results;
		  } catch(Exception e){
			  System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				//System.exit(0);
			  }
			  System.out.println("Operation for delete sprawa done successfully"); 
			  return null;
	  
		  
	  }
	*/  
		  
/*	  public void selectSprawaPrzezNumer(String id, String numerSprawy){
		  Connection c = null;
		  Statement stmt = null;
		  try{
			  Class.forName("org.sqlite.JDBC");
			  c = DriverManager.getConnection("jdbc:sqlite:test.db");
			  c.setAutoCommit(false);
			  System.out.println("Opened database successfully");
			  
			  stmt = c.createStatement();
			  ResultSet rs = stmt.executeQuery( "SELECT '"+numerSprawy+"';" );
			  while(rs.next()){
				  String id = rs.getString("id");
			      String numerSprawyTemp = rs.getString("numersprawy");
			      List<String> results = new LinkedList<String>();
			      results.add(id);
			      results.add(numerSprawy);
			      
			      
			    //  List<String> results = {id:numersprawy}
			      //int na string; numersprawy
				 /* String  rodzajsprawy = rs.getString("rodzajsprawy");
			         String sadwlasciwy  = rs.getString("sadwlasciwy");
			         String strona = rs.getString("strona");
			         String stronaPrzeciwna = rs.getString("stronaprzeciwna");
			         String polozenie = rs.getString("polozenie");
			         String notatki = rs.getString("notatki");
			         String status = rs.getString("status");
			         String zalacznik = rs.getString("zalacznik");
			         String kalendarz = rs.getString("kalendarz");
			         */
			     
			        
			   //   }
	//		  rs.close();
	//		  stmt.close();
	//		  c.close();
	//		  } catch(Exception e){
	//			  System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	//			  System.exit(0);
	//	  }
	//	  System.out.println("Operation for sprawa done successfully"); 
	//  }

/*
	public void closeConnection(){
		try{
			conn.close();
		} catch (SQLException e){
			System.err.println("Problem z zamknieciem");
			e.printStackTrace();
		}
	}
	*/
	
	
}
