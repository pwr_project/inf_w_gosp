﻿Póki co są tutaj dwa „schematy” - ekran logowania / okno główne i ekran dodawania sprawy. 
Tutaj pytanie: Czy ekran logowania ma być pierwszą rzeczą po włączeniu aplikacji (osobne okienko), czy też ma się wyświetlać w oknie głównym? 
Drugie pytanie: Jakie pola ma posiadać ekran dodawania sprawy? 
Trzecie pytanie: Co ma wchodzić w skład menu głownego? Póki co mam:
1. Dodaj sprawę
2. Szukaj spraw / sprawy
3. Usuń sprawę
4. Zakończ

Reszta funkcjonalności później.

<Maciek>
Jedno okno, zawsze najpierw ekran logowania (uproszczony - 3 przyciski "Adwokat", "Pracownik", "Praktykant").
Dla mnie menu główne z Twoimi 4 punktami jest OK.
Formularz z danymi:
a. Dane sprawy
	i. sygnatura (string),
	ii. rodzaj sprawy (text field ? - karna, cywilna, rozwodowa, podział majątku itd.),
	iii. sąd właściwy (nazwa i adres),
	iv. strona (przycisk -> otwieramy nowe okienko, wybieramy typ podmiotu (patrz law_firm\src\lawFirm\Subject) i wtedy mamy formularz z do wprowadzenia danych podmiotu),
	v. strona przeciwna (jw),
	vi. położenie akt (typu pokoj/regal/numer),
	vii. notatki (text field),
	viii. status sprawy (w toku, zakonczona).

b. Terminy
	i. możliwość ustawienia przypomnienia - to bedzie osobny watek sprawdzajacy co 5sek. w bazie czy jest jakis termin do przypomnienia - wyskakujace okienko,
c. Załączniki (pisma)
	i. prezentacja w kolejności dodania - klikasz "Dodaj plik" i w okienku wyswietla sie nazwa załączonego pliku.
